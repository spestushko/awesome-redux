const ADD_AMOUNT_ON_ACCOUNT = "ADD_AMOUNT_ON_ACCOUNT"
const REMOVE_AMOUNT_FROM_ACCOUNT = "REMOVE_AMOUNT_FROM_ACCOUNT"

function addAmountOnAccount(amount) {
  return {
    amount, 
    ADD_AMOUNT_ON_ACCOUNT
  }
}

function removeAmountFromAccount(amount) {
  return {
    amount, 
    REMOVE_AMOUNT_FROM_ACCOUNT 
  }
}

// Simulates PayPal account verification
function verifyAccount() {
  return (dispatch) => {
    dispatch(removeAmountFromAccount(0.30))

    // Simulate async treatment
    setTimeout(() => {
      dispatch(addAmountOnAccount(0.30))
    }, 1000)
  }
}

module.exports = {
  ADD_AMOUNT_ON_ACCOUNT, 
  REMOVE_AMOUNT_FROM_ACCOUNT, 
  
  addAmountOnAccount, 
  removeAmountFromAccount, 
  verifyAccount
}
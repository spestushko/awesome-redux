class Store{
  constructor(reducers) {
    this._reducers = reducers
    this._state = {}
    this.subscribers = []

    // Don't let dispatch() to lose it's context
    this.dispatch = this.dispatch.bind(this)

    // Init call to create store
    this.dispatch({ type: '__INIT__' })
  }

  _dispatch(action) {
    // Get all reducer names
    const reducerNames = Object.keys(this._reducers)

    // Computes new program state by propagating the action
    const nextState = reducerNames.reduce((newState, name) => {

      // Calls each reducer with current state and action
      newState[name] = this._reducers[name](this._state[name], action)

      return newState

    }, {})

    // Keep new state in memory
    this._state = nextState
  }

  dispatch(action) {
    if (typeof action === 'function') {
      return action(this.dispatch)
    }

    this._dispatch(action)
  }

  subscribe(subscriber) {
      this.subscribers.push(subscriber)

      // Return unsubscribe method to remove subscriber from the array of subscribers
      return () => {
        this.subscribers.splice(this.subscribers.indexOf(subscriber), 1)
      }
  }

  getState() {
    return this._state
  }
}